# Guess commit

Use brute-force to generate a git commit with a known prefix. It uses an existing commit and amends it.

There are two approaches:
- update the text of the commit messages to get a commit with a known prefix.
- update a file (blob) to get a commit with a known prefix.

This is a project I created to learn to use Rust. Code probably won't respect all Rust's idioms at the moment
(for example, byte handling using `Vec<u8>` might not be the best), but good enough for a first iteration `:)` of a PoC.

To learn more about Git internals: https://git-scm.com/book/en/v2/Git-Internals-Git-Objects

## How to use it

Build it and run:

    ./target/release/main \
        --repository $(TEST_REPOSITORY) \
        --commit $(TEST_COMMIT) \
        --max-iterations $(TEST_ITERATIONS) \
        --prefix $(TEST_PREFIX_SHORT)

For example, you can use the test-repository (used to run unittest):

    $ tar xzf test-repo.tgz
    $ ./target/release/main \
        --repository "./test-repo" \
        --commit "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b" \
        --max-iterations "10000000" \
        --prefix "c0d3"

After a few seconds you'll get a commit that starts with `c0d3`:

    $ git -C test-repo/ log -n1 
    commit c0d3d3b5a0d81c931e93325ae64776cddb6ba8e3 (HEAD -> master)
    Author: Horacio G. de Oro <hgdeoro@gmail.com>
    Date:   Sat Oct 22 23:10:51 2022 +0200
    
        Update file2
        
        Second line
        <RRk-tcX;Gp=D1H$>y.'5dD_3Ct,F/<V3aQ>t*G5>@C"c'hKKi0Teo%>F4uH

Instead of changing the commit message, the brute-force approach can work by
using random blob (file contents), just add `--filename`:

    $ ./target/release/main \
        --repository "./test-repo" \
        --commit "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b" \
        --max-iterations "10000000" \
        --prefix "c0d3" \
        --filename "file2"


## Run unittests


Since there is a single *test repository*, is impossible to run test concurrently. Remember to add `--test-threads=1`

    $ cargo test -- --test-threads=1

or better use `make test` (which takes care of uncompressing the test repository).

## Build release version

**Release** versions are **much faster** than **debug** version. 

    $ cargo build
    $ ./target/debug/main (...)
    (...)
     - performance: 6857.72 iterations/sec

    $ make release
    $ ./target/release/main (...)
    (...)
     - performance: 338577.49 iterations/sec

Debug build: 6k iterations/sec. Release build: 338k iterations/sec.

## Profiling

    $ perf record --call-graph=dwarf \
        ./target/release/rust-learn-git ./test-repo a2d4044ee8bdeb0d6620134d94b13584ed80bd2b 10000000 0002
    $ perf script --fields +pid > perf.data-firefox-profiler.txt

or use the `make` targets.

![](firefox-profiler.jpg)

## Ideas

* Shouldn't be difficult to create a new commit instead of amending an existing one.
* Handle mutation of child tree objects.

## Learning Rust / TODOs

* Use `#[bench]` for benchmarks.
* Linter suggested to use `&[u8]` instead of `&Vec<u8>`. TODO: look for other optimization / simplifications opportunities.
