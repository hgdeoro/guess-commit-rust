TEST_REPOSITORY ?= "./test-repo"
TEST_COMMIT ?= "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b"
TEST_COMMIT_APPEND_BLOB ?= "c7a2ca90d11144ecfcafc77e0eed03fcd7ae244b"
TEST_ITERATIONS ?=  "10000000"
TEST_PREFIX_SHORT ?= "cafe"
TEST_PREFIX_LONG ?= "cafebabe"
TEST_FILE ?= "file2"
TEST_FILE_APPEND_BLOB ?= "installer.tar.gz"

test:
	test -d $(TEST_REPOSITORY) || tar xzf test-repo.tgz
	RUST_BACKTRACE=1 cargo test -- --test-threads=1

lint:
	cargo fmt
	cargo clippy

release:
	cargo build --release

run: run-mutate-commit-short run-mutate-blob-short run-append-blob-short

# ------------------------------------------------------------------------------------------
# mutate-commit
# ------------------------------------------------------------------------------------------

run-mutate-commit: run-mutate-commit-short

run-mutate-commit-short: release
	./target/release/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT) --max-iterations $(TEST_ITERATIONS) --prefix $(TEST_PREFIX_SHORT)

run-mutate-commit-long: release
	./target/release/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT) --max-iterations $(TEST_ITERATIONS) --prefix $(TEST_PREFIX_LONG)

# ------------------------------------------------------------------------------------------
# mutate-blob
# ------------------------------------------------------------------------------------------

run-mutate-blob: run-mutate-blob-short

run-mutate-blob-short: release
	./target/release/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT) --max-iterations $(TEST_ITERATIONS) --prefix $(TEST_PREFIX_SHORT) --filename $(TEST_FILE)

run-mutate-blob-long: release
	./target/release/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT) --max-iterations $(TEST_ITERATIONS) --prefix $(TEST_PREFIX_LONG) --filename $(TEST_FILE)

# ------------------------------------------------------------------------------------------
# append-blob
# ------------------------------------------------------------------------------------------

run-append-blob: run-append-blob-short

run-append-blob-short: release
	./target/release/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT_APPEND_BLOB) --max-iterations $(TEST_ITERATIONS) --prefix $(TEST_PREFIX_SHORT) --filename $(TEST_FILE_APPEND_BLOB) --file-append-size 64

run-append-blob-long: release
	./target/release/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT_APPEND_BLOB) --max-iterations $(TEST_ITERATIONS) --prefix $(TEST_PREFIX_LONG) --filename $(TEST_FILE_APPEND_BLOB) --file-append-size 64

# ------------------------------------------------------------------------------------------
# profile
# ------------------------------------------------------------------------------------------

profile-build:
	cargo build --profile release-debug-info

profile-mutate-commit: profile-build
	perf record --call-graph=dwarf \
		./target/release-debug-info/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT) --max-iterations $(TEST_ITERATIONS) \
			--prefix $(TEST_PREFIX_SHORT)

profile-mutate-blob: profile-build
	perf record --call-graph=dwarf \
		./target/release-debug-info/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT) --max-iterations $(TEST_ITERATIONS) \
			--prefix $(TEST_PREFIX_SHORT) --filename $(TEST_FILE)

profile-append-blob: profile-build
	perf record --call-graph=dwarf \
		./target/release-debug-info/main --repository $(TEST_REPOSITORY) --commit $(TEST_COMMIT_APPEND_BLOB) --max-iterations $(TEST_ITERATIONS) \
			--prefix $(TEST_PREFIX_SHORT) --filename $(TEST_FILE_APPEND_BLOB) --file-append-size 64

# ------------------------------------------------------------------------------------------
# other
# ------------------------------------------------------------------------------------------

profile-perf-data-to-firefox-profiler:
	perf script --fields +pid > perf.data-firefox-profiler-$(shell date +%s).txt

sudo-enable-perf:
	echo 2 | sudo tee /proc/sys/kernel/perf_event_paranoid

test-ignored-on-taskset:
	taskset -c 7,8,9,10,11,12 cargo test -- --nocapture --ignored
