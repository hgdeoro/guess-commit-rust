use anyhow::Result;
use rand::rngs::StdRng;
use rand_core::RngCore;
use sha1::digest::FixedOutputReset;
use sha1::{self, Digest};
use std::fmt;

use crate::git_base::DynaGitObject;
use crate::utils::{prettify, write_hex_string, MUT_BEGIN, MUT_WIDTH};

/// Main structure, contains the bytes for the whole commit object (HEADER + CONTENTS).
/// This is the most efficient approach to make calculation of sha1 and generation of random text
/// for the commit message as fast as possible.
///
/// Some terminology:
/// - Git stores different TYPES of OBJECTS: COMMITS objects, TREE objects, etc.
/// - COMMITS objects are formed by a HEADER plus the CONTENTS
/// - The SHA1 of the commit is calculated using the whole object's bytes (HEADER + CONTENTS)
pub struct GitCommitObject {
    pub buffer: Vec<u8>,
    pub mutable_zone_start: usize,
    pub mutable_zone_size: usize,
    sha1_hasher: sha1::Sha1,
    rng: StdRng,
    index_tree_sha1: usize,
}

impl DynaGitObject for GitCommitObject {}

impl fmt::Debug for GitCommitObject {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pretty_buffer = prettify(&self.buffer);
        f.debug_struct("GitCommitObject")
            .field("buffer", &pretty_buffer)
            .field("mutable_zone_start", &self.mutable_zone_start)
            .field("mutable_zone_size", &self.mutable_zone_size)
            .field("index_tree_sha1", &self.index_tree_sha1)
            .finish()
    }
}

impl GitCommitObject {
    /// Create a GitCommitObject and returns it.
    pub fn from_repository(repository: &git2::Repository, oid: &git2::Oid) -> Result<GitCommitObject> {
        let internal_buffer = GitCommitObject::create_buffer(repository, oid)?;
        // we will need `mutable_zone_start` & `mutable_zone_size` only if we're mutating commit message
        let (mutable_zone_start, mutable_zone_size) =
            get_mutable_zone_start_and_len(&internal_buffer).unwrap_or((0, 0));
        let tree_sha1 = hex::encode(repository.find_commit(*oid)?.tree_id().as_bytes());
        // we will need `index_tree_sha1` only if we're mutating blob
        let index_tree_sha1 = Self::find_sha1_string_in_buffer(&internal_buffer, tree_sha1)?;

        let sha1_hasher = sha1::Sha1::new();
        let rng = GitCommitObject::create_rng();

        Ok(GitCommitObject {
            buffer: internal_buffer,
            mutable_zone_start,
            mutable_zone_size,
            sha1_hasher,
            rng,
            index_tree_sha1,
        })
    }

    /// Calculates sha1 of the commit object
    pub fn sha1(&mut self) -> Vec<u8> {
        self.sha1_hasher.update(&self.buffer);
        self.sha1_hasher.finalize_fixed_reset().to_vec()
    }

    /// Calculates sha1 and verifies if generated commit matches the prefix we're looking for
    pub fn calculate_sha1_and_compare(&mut self, expected_prefix: &Vec<u8>) -> bool {
        let generated_commit = self.sha1();
        for i in 0..expected_prefix.len() {
            if generated_commit[i] != expected_prefix[i] {
                return false;
            }
        }
        true
    }

    /// Update the mutable zone. Introduces changes to generate a new sha1.
    pub fn update_mutable_zone(&mut self) {
        debug_assert_ne!(self.mutable_zone_start, 0);
        debug_assert_ne!(self.mutable_zone_size, 0);

        // first let's fill random bytes
        let dest = &mut self.buffer[self.mutable_zone_start..self.mutable_zone_start + self.mutable_zone_size];
        self.rng.fill_bytes(dest);
        // now let's make sure all bytes are in the safe ascii range
        for value in &mut self.buffer[self.mutable_zone_start..self.mutable_zone_start + self.mutable_zone_size] {
            *value = MUT_BEGIN + (*value % MUT_WIDTH);
        }
    }

    /// Returns the commit object CONTENTS (that is, excluding the HEADER)
    pub fn get_commit_object_contents(&self) -> Result<&[u8]> {
        Self::contents_from_buffer(&self.buffer)
    }

    pub fn update_tree_sha1_bytes(&mut self, new_file_sha1_bytes: Vec<u8>) {
        debug_assert_ne!(self.index_tree_sha1, 0);
        debug_assert_eq!(new_file_sha1_bytes.len(), 20);
        write_hex_string(&mut self.buffer[self.index_tree_sha1..self.index_tree_sha1 + 40], &new_file_sha1_bytes);
    }
}

/// Returns information to locate mutable zone inside the buffer.
/// The mutable zone is the zone that initially contains consecutive '*'.
/// That zone will be replaced with random ASCII characters.
pub fn get_mutable_zone_start_and_len(buffer: &Vec<u8>) -> Option<(usize, usize)> {
    let mut start_pos = 0;
    let mut count = 0;
    for (pos, value) in buffer.iter().enumerate() {
        if *value == b'*' {
            if count == 0 {
                // we found first one
                count = 1;
                start_pos = pos;
            } else {
                // we found a serie :)
                count += 1;
            }
        } else {
            // not a '*', maybe we found the end of it
            if pos - start_pos >= 10 {
                // yeap, we found the end of it :D
                return Some((start_pos, pos - start_pos));
            } else {
                count = 0;
                start_pos = pos;
            }
        }
    }
    if buffer.len() - start_pos >= 10 {
        Some((start_pos, buffer.len() - start_pos))
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use std::error::Error;

    use git2::{self};

    use crate::utils::{reset_repository, MUT_END_INCLUSIVE};

    use super::*;

    const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";
    const TREE_SHA1: &str = "dc58401aab97aa0d1c279265d535c296d6c4c9f9";
    const BLOB_SHA1: &str = "7a4596f15adadab08ef8b71594d3692f177fce9e";

    #[test]
    fn validate_test_repository() -> Result<(), Box<dyn Error>> {
        let repository = reset_repository("./test-repo", COMMIT_HEAD)?;
        let odb = repository.odb()?;

        let oid = git2::Oid::from_str(COMMIT_HEAD).unwrap();
        let odb_object = odb.read(oid).unwrap();
        assert_eq!(odb_object.data().len(), 309);

        let oid = git2::Oid::from_str(TREE_SHA1).unwrap();
        let odb_object = odb.read(oid).unwrap();
        assert_eq!(odb_object.data().len(), 66);

        let oid = git2::Oid::from_str(BLOB_SHA1).unwrap();
        let odb_object = odb.read(oid).unwrap();
        assert_eq!(odb_object.data().len(), 7);

        Ok(())
    }

    #[test]
    fn test_from_repository() -> Result<()> {
        let repository = reset_repository("./test-repo", COMMIT_HEAD)?;
        let commit_oid = git2::Oid::from_str(COMMIT_HEAD)?;
        let mut dyna_commit = GitCommitObject::from_repository(&repository, &commit_oid)?;

        assert_eq!(
            dyna_commit.buffer,
            b"commit 309\x00\
            tree dc58401aab97aa0d1c279265d535c296d6c4c9f9\n\
            parent 3abdd98c08a881ae47743b01c9afdc92a1702f09\n\
            author Horacio G. de Oro <hgdeoro@gmail.com> 1666473051 +0200\n\
            committer Horacio G. de Oro <hgdeoro@gmail.com> 1666481957 +0200\n\
            \n\
            Update file2\n\
            \n\
            Second line\n\
            ************************************************************\n"
        );
        assert_eq!(dyna_commit.mutable_zone_start, 259);
        assert_eq!(dyna_commit.mutable_zone_size, 60);

        // Make sure we reproduce the original sha1
        assert_eq!(hex::encode(dyna_commit.sha1()), COMMIT_HEAD);
        Ok(())
    }

    #[test]
    fn test_update_tree_sha1() -> Result<()> {
        let repository = reset_repository("./test-repo", COMMIT_HEAD)?;
        let commit_oid = git2::Oid::from_str(COMMIT_HEAD)?;
        let mut dyna_commit = GitCommitObject::from_repository(&repository, &commit_oid)?;

        dyna_commit.update_tree_sha1_bytes(
            b"\x00\x01\x00\x01\x00\x01\x00\x01\x00\x01\x00\x01\x00\x01\x00\x01\x00\x01\x00\x01".to_vec(),
        );

        assert_eq!(
            prettify(&dyna_commit.buffer),
            prettify(
                b"commit 309\x00\
            tree 0001000100010001000100010001000100010001\n\
            parent 3abdd98c08a881ae47743b01c9afdc92a1702f09\n\
            author Horacio G. de Oro <hgdeoro@gmail.com> 1666473051 +0200\n\
            committer Horacio G. de Oro <hgdeoro@gmail.com> 1666481957 +0200\n\
            \n\
            Update file2\n\
            \n\
            Second line\n\
            ************************************************************\n"
            )
        );

        Ok(())
    }

    #[test]
    fn test_update_mutable_zone_use_full_widh_of_allowed_ascii() -> Result<()> {
        let repository = reset_repository("./test-repo", COMMIT_HEAD)?;
        let commit_oid = git2::Oid::from_str(COMMIT_HEAD)?;
        let mut dyna_commit = GitCommitObject::from_repository(&repository, &commit_oid)?;

        let mut max = MUT_BEGIN;
        let mut min = MUT_END_INCLUSIVE;
        for iter in 0..100_000 {
            dyna_commit.update_mutable_zone();
            for i in 0..dyna_commit.mutable_zone_size {
                assert!(dyna_commit.buffer[dyna_commit.mutable_zone_start + i] >= MUT_BEGIN);
                assert!(dyna_commit.buffer[dyna_commit.mutable_zone_start + i] <= MUT_END_INCLUSIVE);
                if max < dyna_commit.buffer[dyna_commit.mutable_zone_start + i] {
                    max = dyna_commit.buffer[dyna_commit.mutable_zone_start + i];
                }
                if min > dyna_commit.buffer[dyna_commit.mutable_zone_start + i] {
                    min = dyna_commit.buffer[dyna_commit.mutable_zone_start + i];
                }
            }
            if iter > 1000 && min == MUT_BEGIN && max == MUT_END_INCLUSIVE {
                break;
            }
        }
        assert_eq!(max, MUT_END_INCLUSIVE);
        assert_eq!(min, MUT_BEGIN);

        Ok(())
    }
}
