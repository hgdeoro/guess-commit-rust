extern crate core;

use anyhow::Result;
use std::time::{self};

use git2::{self};

use crate::git_commit;
use crate::mutate_result::{GuessCommitError, MutateResult};
use crate::utils::prettify;

pub fn guess_by_mutating_commit(
    repository: git2::Repository,
    commit_oid: git2::Oid,
    max_iterations: u64,
    wanted_prefix: Vec<u8>,
    dry_run: bool,
) -> Result<MutateResult> {
    let mut buffer = git_commit::GitCommitObject::from_repository(&repository, &commit_oid)?;
    if buffer.mutable_zone_size == 0 || buffer.mutable_zone_start == 0 {
        println!("ERROR: mutable zone not found");
        println!("buffer={}", prettify(&buffer.buffer));
        println!("mutable_zone_start={}", buffer.mutable_zone_start);
        println!("mutable_zone_size={}", buffer.mutable_zone_size);
        return Err(anyhow::anyhow!(GuessCommitError::MutableZoneNotFound));
    }

    let prog_start_time = time::Instant::now();
    let mut prog_last_print: u64 = 0;
    let mut iterations = 0;
    loop {
        iterations += 1;

        // let's change the commit message so we can generate a new sha1
        buffer.update_mutable_zone();

        // calculate sha1 and check if we found a match
        if buffer.calculate_sha1_and_compare(&wanted_prefix) {
            break;
        }

        if iterations >= max_iterations {
            return Err(anyhow::anyhow!(GuessCommitError::MaxIterationsError));
        }
        if iterations % 10000 == 0 {
            let elapsed = prog_start_time.elapsed().as_secs_f64();
            let elapsed_int = elapsed as u64;
            if elapsed_int > prog_last_print {
                prog_last_print = elapsed_int;
                println!(" + so far: {:.2?} iterations/sec", iterations as f64 / elapsed);
            }
        }
    }

    let new_oid;
    if dry_run {
        println!("Won't write commit (--dry-run)");
        new_oid = git2::Oid::from_bytes(&buffer.sha1())?;
    } else {
        // Insert commit object with contents from buffer
        let odb = repository.odb()?;
        new_oid = odb.write(git2::ObjectType::Commit, buffer.get_commit_object_contents()?)?;

        // Update HEAD to point to the commit we just created
        repository
            .reset(repository.find_commit(new_oid)?.as_object(), git2::ResetType::Soft, Option::None)
            .expect("amend failed");
    }

    return Ok(MutateResult {
        commit_sha1: new_oid.as_bytes().to_vec(),
        iterations,
        duration_sec: prog_start_time.elapsed().as_secs_f64(),
    });
}

#[cfg(test)]
mod tests {
    use crate::utils;

    use super::*;

    const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";
    const COMMIT_EXPECTED: &str = "cae87b7c5b8daffa1d637fa8302177e9ef162fe3";

    #[test]
    fn test_guess_commit() -> Result<()> {
        std::env::set_var("RNG_TYPE", "PREDICTABLE");

        let repository = utils::reset_repository("./test-repo", COMMIT_HEAD)?;
        let commit_oid = git2::Oid::from_str(COMMIT_HEAD)?;
        let max_iterations = 1000;
        let wanted_prefix = hex::decode("ca")?;
        let result = guess_by_mutating_commit(repository, commit_oid, max_iterations, wanted_prefix, false)?;
        assert_eq!(result.commit_sha1, hex::decode(COMMIT_EXPECTED)?);
        Ok(())
    }
}
