use std::env;

use anyhow::{anyhow, Result};
use git2::{self};
use rand::rngs::StdRng;
use rand_core::SeedableRng;

pub trait DynaGitObject {
    /// Generate the git object's header, including final `\x00`
    fn get_object_header_bytes(object: &git2::OdbObject) -> Result<Vec<u8>> {
        match object.kind() {
            git2::ObjectType::Tree => Ok(format!("tree {}\0", object.len()).as_bytes().to_vec()),
            git2::ObjectType::Blob => Ok(format!("blob {}\0", object.len()).as_bytes().to_vec()),
            git2::ObjectType::Commit => Ok(format!("commit {}\0", object.len()).as_bytes().to_vec()),
            other => Err(anyhow!("Unexpected type of OdbObject. type={:?}", other)),
        }
    }

    /// Creates and populates the vector that will act as internal buffer.
    /// This doesn't need to be efficient, it's done only once.
    fn create_buffer(repository: &git2::Repository, oid: &git2::Oid) -> Result<Vec<u8>> {
        let odb = repository.odb()?;

        let object = odb.read(*oid)?;
        let object_bytes = object.data();
        assert_eq!(object.len(), object_bytes.len()); // we assume this on get_object_header_bytes()

        let mut internal_buffer = vec![];
        internal_buffer.extend(Self::get_object_header_bytes(&object)?);
        internal_buffer.extend(object_bytes);
        Ok(internal_buffer)
    }

    /// Returns slice with the contents of the git object (that means, excluding the header)
    fn contents_from_buffer(buffer: &[u8]) -> Result<&[u8]> {
        // TODO: maybe we should validate the ignored bytes?
        match buffer.iter().position(|&x| x == b'\x00') {
            Some(first_null) => Ok(&buffer[first_null + 1..]),
            _ => Err(anyhow!("NULL not found in commit object")),
        }
    }

    fn find_sha1_string_in_buffer(buffer: &Vec<u8>, to_find: String) -> Result<usize> {
        let to_find_bytes = to_find.as_bytes();
        assert_eq!(to_find.len(), 40, "Text to find is expected to be an sha1 with len=40");
        assert_eq!(to_find_bytes.len(), 40, "Byte representation of sha1 is expected to have len=40");

        'outer: for i in 0..(buffer.len() - 39) {
            for (j, hex_byte) in to_find_bytes.iter().enumerate() {
                if buffer[i + j] != *hex_byte {
                    continue 'outer;
                }
            }
            return Ok(i);
        }
        Err(anyhow!("sha1={} not found in content={:?}", to_find, String::from_utf8_lossy(buffer)))
    }

    fn find_sha1_bytes_in_buffer(buffer: &Vec<u8>, to_find: &Vec<u8>) -> Result<usize> {
        assert_eq!(to_find.len(), 20, "Bytes to find is expected to be an sha1 with len=20");
        'outer: for i in 0..(buffer.len() - 19) {
            for (j, hex_byte) in to_find.iter().enumerate() {
                if buffer[i + j] != *hex_byte {
                    continue 'outer;
                }
            }
            return Ok(i);
        }
        Err(anyhow!("sha1={:?} not found in content={:?}", hex::encode(to_find), String::from_utf8_lossy(buffer)))
    }

    fn create_rng() -> StdRng {
        match env::var("RNG_TYPE") {
            Ok(value) => match value.as_str() {
                "PREDICTABLE" => StdRng::seed_from_u64(0),
                _ => match value.parse::<u64>() {
                    Ok(value) => {
                        println!("Seeding StdRng with {}", value);
                        StdRng::seed_from_u64(value)
                    }
                    _ => StdRng::from_entropy(),
                },
            },
            Err(_) => StdRng::from_entropy(),
        }
    }
}
