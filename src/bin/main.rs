use clap::error::ErrorKind;
use clap::{CommandFactory, Parser};

use guess_commit::mutate_blob::guess_by_mutating_blob;
use guess_commit::mutate_blob_append::guess_by_append_to_blob;
use guess_commit::mutate_commit::guess_by_mutating_commit;
use guess_commit::mutate_result::{self};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long, value_parser = repository_is_valid)]
    repository: String,

    #[arg(short, long, value_parser = prefix_is_valid)]
    commit: String,

    #[arg(short, long)]
    max_iterations: u64,

    #[arg(short, long, value_parser = prefix_is_valid)]
    prefix: String,

    #[arg(short, long)]
    filename: Option<String>,

    #[arg(short, long)]
    blob_size: Option<usize>,

    #[arg(short, long)]
    file_append_size: Option<usize>,

    #[arg(short, long, default_value = "false")]
    dry_run: Option<bool>,
}

fn repository_is_valid(value: &str) -> Result<String, String> {
    match git2::Repository::open(value) {
        Ok(_repository) => Ok(String::from(value)),
        Err(err) => Err(format!("{}", err)),
    }
}

fn prefix_is_valid(value: &str) -> Result<String, String> {
    match hex::decode(value) {
        Ok(_hex) => Ok(String::from(value)),
        Err(err) => Err(format!("{}", err)),
    }
}

fn validate_commit(args: &Args, repository: &git2::Repository) -> git2::Oid {
    match git2::Oid::from_str(&args.commit) {
        Ok(oid) => {
            if let Err(err) = repository.find_commit(oid) {
                let mut cmd = Args::command();
                cmd.error(
                    ErrorKind::InvalidValue,
                    format!("Commit invalid or doesn't exists: {} ({})", args.commit, err.message()),
                )
                .exit();
            };
            oid
        }
        Err(_) => {
            let mut cmd = Args::command();
            cmd.error(ErrorKind::InvalidValue, format!("Invalid commit: {}", args.commit)).exit()
        }
    }
}

fn reset(repository: &git2::Repository, commit_oid: &git2::Oid) {
    let commit = repository.find_commit(*commit_oid).unwrap();
    let target = commit.as_object();
    repository.reset(target, git2::ResetType::Soft, None).expect("Failed to reset HEAD");

    // Validate HEAD points to right commit (the commit we want to amend)
    if repository.head().unwrap().target().unwrap() != *commit_oid {
        let mut cmd = Args::command();
        cmd.error(ErrorKind::InvalidValue, format!("Repository HEAD doesn't refers to commit {}", commit_oid)).exit();
    }
}

fn main() {
    let args = Args::parse();

    let repository = git2::Repository::open(&args.repository).expect("Couldn't open git repository");
    let commit_oid = validate_commit(&args, &repository);
    reset(&repository, &commit_oid);

    let wanted_prefix: Vec<u8> = hex::decode(&args.prefix).unwrap();
    let max_iterations = args.max_iterations;
    let dry_run = args.dry_run.unwrap_or(false);

    let result = match (args.filename, args.blob_size, args.file_append_size) {
        (Some(filename), Some(blob_size), None) => {
            guess_by_mutating_blob(repository, commit_oid, max_iterations, wanted_prefix, dry_run, &filename, blob_size)
        }
        (Some(filename), None, None) => {
            let blob_size = 64;
            guess_by_mutating_blob(repository, commit_oid, max_iterations, wanted_prefix, dry_run, &filename, blob_size)
        }
        (Some(filename), None, Some(file_append_size)) => guess_by_append_to_blob(
            repository,
            commit_oid,
            max_iterations,
            wanted_prefix,
            dry_run,
            &filename,
            file_append_size,
        ),
        (None, None, None) => guess_by_mutating_commit(repository, commit_oid, max_iterations, wanted_prefix, dry_run),
        _ => {
            let mut cmd = Args::command();
            let msg = "Invalid combination of arguments: ";
            cmd.error(ErrorKind::InvalidValue, msg).exit();
        }
    };

    match result {
        Ok(result) => {
            println!(
                "SUCCESS: commit_sha1={} iterations={} duration_sec={:.2}",
                hex::encode(result.commit_sha1),
                result.iterations,
                result.duration_sec
            );
            println!(" - performance: {:.2?} iterations/sec", result.iterations as f64 / result.duration_sec);
            std::process::exit(0);
        }
        Err(err) => match err.downcast().unwrap() {
            mutate_result::GuessCommitError::InvalidRepository => {
                println!("ERROR: invalid git repository");
                std::process::exit(1);
            }
            mutate_result::GuessCommitError::MaxIterationsError => {
                println!("ERROR: couldn't find commit after {} iterations", args.max_iterations);
                std::process::exit(1);
            }
            mutate_result::GuessCommitError::InvalidCurrentCommit => {
                println!("ERROR: repository HEAD is not the expected {}", commit_oid);
                std::process::exit(1);
            }
            mutate_result::GuessCommitError::InternalError => {
                println!("ERROR: unexpected error detected");
                std::process::exit(2);
            }
            mutate_result::GuessCommitError::MutableZoneNotFound => {
                println!("ERROR: mutable zone not found in commit message.");
                std::process::exit(2);
            }
        },
    }
}
