use anyhow::Result;
use std::fmt;

use sha1::digest::FixedOutputReset;
use sha1::{self, Digest};

use crate::git_base::DynaGitObject;
use crate::utils::prettify;

pub struct GitTreeObject {
    pub buffer: Vec<u8>,
    sha1_hasher: sha1::Sha1,
    index_file_sha1: usize,
}

impl DynaGitObject for GitTreeObject {}

impl fmt::Debug for GitTreeObject {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pretty_buffer = prettify(&self.buffer);
        f.debug_struct("GitTreeObject")
            .field("buffer", &pretty_buffer)
            .field("index_file_sha1", &self.index_file_sha1)
            .finish()
    }
}

impl GitTreeObject {
    pub fn from_repository(repository: &git2::Repository, oid: &git2::Oid, filename: String) -> Result<GitTreeObject> {
        let tree_id = repository.find_commit(*oid)?.tree_id();
        let internal_buffer = Self::create_buffer(repository, &tree_id)?;
        let blob_sha1 = Self::get_sha1_for_file(repository, oid, filename)?;
        let index_file_sha1 = Self::find_sha1_bytes_in_buffer(&internal_buffer, &blob_sha1)?;
        let sha1_hasher = sha1::Sha1::new();
        Ok(GitTreeObject { buffer: internal_buffer, sha1_hasher, index_file_sha1 })
    }

    fn get_sha1_for_file(repository: &git2::Repository, oid: &git2::Oid, filename: String) -> Result<Vec<u8>> {
        let tree_id = repository.find_commit(*oid)?.tree_id();
        let tree = repository.find_tree(tree_id)?;
        let entry = tree.get_name(&filename).unwrap();
        Ok(entry.id().as_bytes().to_vec())
    }

    /// Calculates sha1 of the tree object
    pub fn sha1(&mut self) -> Vec<u8> {
        self.sha1_hasher.update(&self.buffer);
        self.sha1_hasher.finalize_fixed_reset().to_vec()
    }

    /// Update the sha1 of the file
    pub fn update_file_sha1(&mut self, new_file_sha1: Vec<u8>) {
        debug_assert_eq!(new_file_sha1.len(), 20);
        self.buffer[self.index_file_sha1..self.index_file_sha1 + 20].copy_from_slice(&new_file_sha1);
    }

    /// Returns the commit object CONTENTS (that is, excluding the HEADER)
    pub fn get_tree_object_contents(&self) -> Result<&[u8]> {
        Self::contents_from_buffer(&self.buffer)
    }
}

#[cfg(test)]
mod tests {
    use crate::git_tree::GitTreeObject;
    use crate::utils::{prettify, reset_repository};
    use anyhow::Result;

    const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";
    const TREE_SHA1: &str = "dc58401aab97aa0d1c279265d535c296d6c4c9f9";
    const FILE2_SHA1: &str = "7a4596f15adadab08ef8b71594d3692f177fce9e";

    #[test]
    fn test_create_tree_object() -> Result<()> {
        let repository = reset_repository("./test-repo", COMMIT_HEAD)?;
        let oid = git2::Oid::from_str(COMMIT_HEAD)?;
        let filename = String::from("file2");

        let mut dyna_tree = GitTreeObject::from_repository(&repository, &oid, filename)?;
        assert_eq!(hex::encode(dyna_tree.sha1()), TREE_SHA1);
        assert_eq!(hex::encode(&dyna_tree.buffer[54..74]), FILE2_SHA1);
        assert_eq!(dyna_tree.index_file_sha1, 54);

        Ok(())
    }

    #[test]
    fn test_update_file_sha1() -> Result<()> {
        let repository = reset_repository("./test-repo", COMMIT_HEAD)?;
        let oid = git2::Oid::from_str(COMMIT_HEAD)?;
        let filename = String::from("file2");

        let mut dyna_tree = GitTreeObject::from_repository(&repository, &oid, filename)?;
        let expected = "tree 66\\x00100644 file1\\x00\\xe2\\x12\\x97\\x01\\xf1\\xa4\\xd5M\\xc4O\\x03\\xc9;\\xca\n*\\xec|TI100644 file2\\x00zE\\x96\\xf1Z\\xda\\xda\\xb0\\x8e\\xf8\\xb7\\x15\\x94\\xd3i/\\x17\\x7f\\xce\\x9e";
        assert_eq!(prettify(&dyna_tree.buffer), String::from(expected));

        dyna_tree.update_file_sha1(hex::decode("0000000000000000000000000000000000000000")?.to_vec());
        let expected = "tree 66\\x00100644 file1\\x00\\xe2\\x12\\x97\\x01\\xf1\\xa4\\xd5M\\xc4O\\x03\\xc9;\\xca\n*\\xec|TI100644 file2\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00";
        assert_eq!(prettify(&dyna_tree.buffer), String::from(expected));

        dyna_tree.update_file_sha1(hex::decode(FILE2_SHA1)?.to_vec());
        // assert_eq!(prettify(&dyna_tree.buffer), String::from(expected));

        Ok(())
    }
}
