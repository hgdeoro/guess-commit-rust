use clap::error::ErrorKind;
use clap::{CommandFactory, Parser};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long, value_parser = repository_is_valid)]
    repository: String,

    #[arg(short, long, value_parser = prefix_is_valid)]
    commit: String,

    #[arg(short, long)]
    max_iterations: u128,

    #[arg(short, long, value_parser = prefix_is_valid)]
    prefix: String,

    #[arg(short, long)]
    filename: Option<String>,
}

fn repository_is_valid(value: &str) -> Result<String, String> {
    match git2::Repository::open(value) {
        Ok(_repository) => Ok(String::from(value)),
        Err(err) => Err(format!("{}", err)),
    }
}

fn prefix_is_valid(value: &str) -> Result<String, String> {
    match hex::decode(value) {
        Ok(_hex) => Ok(String::from(value)),
        Err(err) => Err(format!("{}", err)),
    }
}

#[allow(unused)]
fn main() {
    let args = Args::parse();
    println!("repository={}", args.repository);
    println!("commit={}", args.commit);
    println!("max_iterations={}", args.max_iterations);
    match args.filename {
        Some(filename) => println!("filename={}", filename),
        None => println!("filename=<NOT SPECIFIED>"),
    };

    let oid = match git2::Oid::from_str(&args.commit) {
        Ok(oid) => oid,
        Err(err) => {
            let mut cmd = Args::command();
            cmd.error(ErrorKind::InvalidValue, format!("Invalid commit: {}", args.commit)).exit();
        }
    };

    let repository = git2::Repository::open(args.repository).unwrap();

    if let Err(err) = repository.find_commit(oid) {
        let mut cmd = Args::command();
        cmd.error(
            ErrorKind::InvalidValue,
            format!("Commit invalid or doesn't exists: {} ({})", args.commit, err.message()),
        )
        .exit();
    };
}
