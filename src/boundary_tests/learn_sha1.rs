#[cfg(test)]
mod tests {
    use std::time::Instant;

    use sha1::digest::{FixedOutput, FixedOutputReset};
    use sha1::{Digest, Sha1};

    pub fn calculate_sha1(buffer: &Vec<u8>) -> Vec<u8> {
        let mut sha1_hasher = Sha1::new();
        sha1_hasher.update(buffer);
        let sha1_result: Vec<u8> = sha1_hasher.finalize().to_vec();
        sha1_result
    }

    pub fn calculate_sha1_ng(sha1_hasher: &mut Sha1, buffer: &Vec<u8>) -> Vec<u8> {
        sha1_hasher.update(buffer);
        sha1_hasher.finalize_fixed_reset().to_vec()
    }

    #[test]
    fn test_calculate_sha1() {
        assert_eq!(hex::encode(calculate_sha1(&vec![1])), "bf8b4530d8d246dd74ac53a13471bba17941dff7");
        assert_eq!(hex::encode(calculate_sha1(&vec![2])), "c4ea21bb365bbeeaf5f2c654883e56d11e43c44e");
    }

    #[test]
    fn test_calculate_sha1_ng() {
        let mut sha1_hasher: Sha1 = Sha1::new();
        assert_eq!(
            hex::encode(calculate_sha1_ng(&mut sha1_hasher, &vec![1])),
            "bf8b4530d8d246dd74ac53a13471bba17941dff7"
        );
        assert_eq!(
            hex::encode(calculate_sha1_ng(&mut sha1_hasher, &vec![2])),
            "c4ea21bb365bbeeaf5f2c654883e56d11e43c44e"
        );
    }

    fn perf_calculate_sha1() {
        let buffer = &vec![0; 400];
        let start_time = Instant::now();
        assert_eq!(hex::encode(calculate_sha1(buffer)), "3d59f8de55a42cc13fb2ebda6de3a5193f2ee561");
        for i in 1_u64.. {
            calculate_sha1(buffer);
            if i % 1000 == 0 {
                let elapsed = start_time.elapsed().as_secs_f32();
                if elapsed > 3.0 {
                    println!("calculate_sha1()={:.2}/sec", (i as f32 / elapsed));
                    break;
                }
            }
        }
    }

    fn perf_calculate_sha1_ng() {
        let mut sha1_hasher: Sha1 = Sha1::new();
        let buffer = &vec![0; 400];
        let start_time = Instant::now();
        assert_eq!(
            hex::encode(calculate_sha1_ng(&mut sha1_hasher, buffer)),
            "3d59f8de55a42cc13fb2ebda6de3a5193f2ee561"
        );
        for i in 1_u64.. {
            calculate_sha1_ng(&mut sha1_hasher, buffer);
            if i % 1000 == 0 {
                let elapsed = start_time.elapsed().as_secs_f32();
                if elapsed > 3.0 {
                    println!("calculate_sha1_ng()={:.2}/sec", (i as f32 / elapsed));
                    break;
                }
            }
        }
    }

    #[test]
    #[ignore]
    fn test_perf_sha1() {
        // calculate_sha1()=12140.03/sec
        // calculate_sha1_ng()=14625.30/sec
        // calculate_sha1()=12005.88/sec
        // calculate_sha1_ng()=14632.18/sec
        // calculate_sha1()=12365.30/sec
        // calculate_sha1_ng()=14794.58/sec
        for _ in 0..3 {
            perf_calculate_sha1();
            perf_calculate_sha1_ng();
        }
    }

    #[test]
    pub fn test_checkpoint() {
        {
            // sha1(one) = fe05bcdcdc4928012781a5f1a2a77cbb5398e106
            let mut sha1_hasher = Sha1::new();
            sha1_hasher.update(b"one");
            assert_eq!(hex::encode(sha1_hasher.finalize()), "fe05bcdcdc4928012781a5f1a2a77cbb5398e106");
        }

        {
            // sha1(one+two) = 30ae97492ce1da88d0e7117ace0a60a6f9e1e0bc
            let mut sha1_hasher = Sha1::new();
            sha1_hasher.update(b"one");
            sha1_hasher.update(b"two");
            assert_eq!(hex::encode(sha1_hasher.finalize()), "30ae97492ce1da88d0e7117ace0a60a6f9e1e0bc");
        }

        {
            // sha1(one+two) = 30ae97492ce1da88d0e7117ace0a60a6f9e1e0bc
            let mut sha1_hasher = Sha1::new();
            sha1_hasher.update(b"onetwo");
            assert_eq!(hex::encode(sha1_hasher.finalize()), "30ae97492ce1da88d0e7117ace0a60a6f9e1e0bc");
        }

        {
            // sha1(one+two) = 30ae97492ce1da88d0e7117ace0a60a6f9e1e0bc
            let mut sha1_hasher_1 = Sha1::new();
            sha1_hasher_1.update(b"one");

            let mut sha1_hasher_2 = sha1_hasher_1.clone();
            sha1_hasher_2.update(b"two");
            assert_eq!(hex::encode(sha1_hasher_2.finalize()), "30ae97492ce1da88d0e7117ace0a60a6f9e1e0bc");

            let mut sha1_hasher_2 = sha1_hasher_1.clone();
            sha1_hasher_2.update(b"two");
            assert_eq!(hex::encode(sha1_hasher_2.finalize()), "30ae97492ce1da88d0e7117ace0a60a6f9e1e0bc");

            assert_eq!(hex::encode(sha1_hasher_1.finalize()), "fe05bcdcdc4928012781a5f1a2a77cbb5398e106");
        }
    }

    fn full_calculate(hasher: &mut Sha1, big_buffer: &[u8], small_suffix: &[u8]) -> Vec<u8> {
        hasher.update(big_buffer);
        hasher.update(small_suffix);
        hasher.finalize_fixed_reset().to_vec()
    }

    fn incremental_init(hasher: &mut Sha1, big_buffer: &[u8]) {
        hasher.update(big_buffer);
    }

    fn incremental_update(hasher: &mut Sha1, small_suffix: &[u8]) -> Vec<u8> {
        let mut cloned = hasher.clone();
        cloned.update(small_suffix);
        cloned.finalize_fixed().to_vec()
    }

    #[test]
    #[ignore]
    fn test_perf_big_sha1_vs_clone() {
        let expected = hex::decode("a7e2608033bc8f3c3e83a0db4066b1e4d4746e89").unwrap();
        let mut sha1_full = Sha1::new();
        let mut sha1_incremental = Sha1::new();

        let big_buffer = vec![127u8; 1024 * 1024];
        let small_buffer = vec![255u8; 32];
        incremental_init(&mut sha1_incremental, &big_buffer);

        for _ in 0..1 {
            {
                //
                // full_calculate()
                //
                let start_time = Instant::now();
                let mut last_print: u64 = 0;
                let mut iter = 0;

                while last_print < 3 {
                    let result_full = full_calculate(&mut sha1_full, &big_buffer, &small_buffer);
                    assert_eq!(result_full, expected);
                    let elapsed = start_time.elapsed().as_secs_f32();
                    if elapsed as u64 > last_print {
                        last_print = elapsed as u64;
                        println!("full_calculate()={:.2}/sec", (iter as f32 / elapsed));
                    }
                    iter += 1;
                }
            }
            {
                //
                // incremental_update()
                //
                let start_time = Instant::now();
                let mut last_print: u64 = 0;
                let mut iter = 0;

                while last_print < 3 {
                    let result_incremental = incremental_update(&mut sha1_incremental, &small_buffer);
                    assert_eq!(result_incremental, expected);
                    let elapsed = start_time.elapsed().as_secs_f32();
                    if elapsed as u64 > last_print {
                        last_print = elapsed as u64;
                        println!("incremental_update()={:.2}/sec", (iter as f32 / elapsed));
                    }
                    iter += 1;
                }
            }
        }
    }
}
