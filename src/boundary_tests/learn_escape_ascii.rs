#[cfg(test)]
mod tests {
    use std::ascii;

    fn prettify(binary_data: &[u8]) -> String {
        let mut result = String::from("");
        for a_byte in binary_data {
            if *a_byte == b'\n' {
                result.push('\n');
            } else {
                for item in ascii::escape_default(*a_byte) {
                    result.push(item as char);
                }
            }
        }
        result
    }

    #[test]
    fn test1() {
        let result = prettify(b"Some\x00\nitem");
        println!("{}", result);
        assert_eq!(result, String::from("Some\\x00\nitem"));
    }
}
