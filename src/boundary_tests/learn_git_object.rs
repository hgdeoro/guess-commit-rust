#[allow(unused)]
#[cfg(test)]
mod tests {
    use std::env;
    use std::error::Error;

    use git2::{ObjectType, Oid, Repository};
    use rand::rngs::StdRng;
    use rand_core::RngCore;
    use rand_core::SeedableRng;
    use sha1::digest::FixedOutputReset;
    use sha1::{self, Digest, Sha1};

    use crate::utils::reset_repository;

    const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";
    const TREE_SHA1: &str = "dc58401aab97aa0d1c279265d535c296d6c4c9f9";
    const BLOB_SHA1: &str = "7a4596f15adadab08ef8b71594d3692f177fce9e";

    pub fn calculate_sha1(buffer: &Vec<u8>) -> Vec<u8> {
        let mut sha1_hasher = Sha1::new();
        sha1_hasher.update(buffer);
        let sha1_result: Vec<u8> = sha1_hasher.finalize().to_vec();
        sha1_result
    }

    #[test]
    fn test_get_objects() -> Result<(), Box<dyn Error>> {
        let repository = &mut Repository::open("./test-repo")?;
        reset_repository("./test-repo", COMMIT_HEAD).expect("Reset failed");
        let odb = repository.odb()?;

        // $ git -C test-repo cat-file -p a2d4044ee8bdeb0d6620134d94b13584ed80bd2b
        // tree dc58401aab97aa0d1c279265d535c296d6c4c9f9
        // parent 3abdd98c08a881ae47743b01c9afdc92a1702f09
        // author Horacio G. de Oro <hgdeoro@gmail.com> 1666473051 +0200
        // committer Horacio G. de Oro <hgdeoro@gmail.com> 1666481957 +0200
        //
        // Update file2
        //
        // Second line
        // ************************************************************

        {
            let commit_object = odb.read(Oid::from_str(COMMIT_HEAD)?)?;
            assert_eq!(commit_object.kind(), ObjectType::Commit);
        }

        // $ git -C test-repo cat-file -p dc58401aab97aa0d1c279265d535c296d6c4c9f9
        // 100644 blob e2129701f1a4d54dc44f03c93bca0a2aec7c5449	file1
        // 100644 blob 7a4596f15adadab08ef8b71594d3692f177fce9e	file2

        {
            let tree_object = odb.read(Oid::from_str(TREE_SHA1)?)?;
            assert_eq!(tree_object.kind(), ObjectType::Tree);

            let tree_object = repository.find_tree(Oid::from_str(TREE_SHA1)?)?;
            let file2_entry = tree_object.get_name("file2").unwrap();
            assert_eq!(file2_entry.name().unwrap(), "file2");

            let odb_object = odb.read(tree_object.id())?;
            let tree_object_contents = Vec::from(odb_object.data());
            let mut tree_object_header: Vec<u8> = vec![];
            tree_object_header.extend(Vec::from("tree "));
            tree_object_header.extend(Vec::from(format!("{}", tree_object_contents.len())));
            let mut tree_object_full: Vec<u8> = vec![];
            tree_object_full.extend(tree_object_header);
            tree_object_full.extend(Vec::from([0]));
            tree_object_full.extend(tree_object_contents);
            assert_eq!(hex::encode(calculate_sha1(&tree_object_full)), TREE_SHA1);
        }

        // $ git -C test-repo cat-file -p 7a4596f15adadab08ef8b71594d3692f177fce9e
        // file2b

        {
            let blob_object = odb.read(Oid::from_str(BLOB_SHA1)?)?;
            assert_eq!(blob_object.kind(), ObjectType::Blob);

            let odb_object = odb.read(blob_object.id())?;
            let blob_object_contents = Vec::from(odb_object.data());
            let mut blob_object_header: Vec<u8> = vec![];
            blob_object_header.extend(Vec::from("blob "));
            blob_object_header.extend(Vec::from(format!("{}", blob_object_contents.len())));
            let mut blob_object_full: Vec<u8> = vec![];
            blob_object_full.extend(blob_object_header);
            blob_object_full.extend(Vec::from([0]));
            blob_object_full.extend(blob_object_contents);
            assert_eq!(hex::encode(calculate_sha1(&blob_object_full)), BLOB_SHA1);
        }

        Ok(())
    }

    #[test]
    fn discover_get_index_sha1_of_blob() -> Result<(), Box<dyn Error>> {
        const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";

        let repository = &mut Repository::open("./test-repo")?;
        reset_repository("./test-repo", COMMIT_HEAD).expect("Reset failed");
        let odb = repository.odb()?;

        const TREE_SHA1: &str = "dc58401aab97aa0d1c279265d535c296d6c4c9f9";
        let tree_object = odb.read(Oid::from_str(TREE_SHA1)?)?;
        assert_eq!(tree_object.kind(), ObjectType::Tree);

        let tree_object = repository.find_tree(Oid::from_str(TREE_SHA1)?)?;
        let file2_entry = tree_object.get_name("file2").unwrap();
        assert_eq!(file2_entry.name().unwrap(), "file2");

        const BLOB_SHA1: &str = "7a4596f15adadab08ef8b71594d3692f177fce9e";

        let odb_object = odb.read(tree_object.id())?;
        let tree_object_contents_bytes = odb_object.data();
        let looking_for = hex::decode(BLOB_SHA1)?;
        assert_eq!(looking_for.len(), 20);

        let mut found = false;
        let mut pos = 0;
        'outer: for i in 0..(tree_object_contents_bytes.len() - 19) {
            for (j, hex_byte) in looking_for.iter().enumerate() {
                if tree_object_contents_bytes[i + j] != *hex_byte {
                    continue 'outer;
                }
            }
            found = true;
            pos = i;
        }

        assert!(found);
        assert!(pos > 0);
        assert_eq!(pos, 46);

        assert_eq!(get_index_sha1(tree_object_contents_bytes, &looking_for), 46);

        Ok(())
    }

    fn get_index_sha1(contents: &[u8], sha1: &Vec<u8>) -> usize {
        'outer: for i in 0..(contents.len() - 19) {
            for (j, hex_byte) in sha1.iter().enumerate() {
                if contents[i + j] != *hex_byte {
                    continue 'outer;
                }
            }
            return i;
        }
        panic!("Couldn't find SHA1 in contents of TREE object");
    }

    pub struct GitTreeObject {
        pub buffer: Vec<u8>,
        sha1_hasher: sha1::Sha1,
        index_file_sha1: usize,
    }

    impl GitTreeObject {
        fn get_object_header_bytes(commit_bytes: &[u8]) -> Vec<u8> {
            format!("tree {}\0", commit_bytes.len()).as_bytes().to_vec()
        }

        /// Creates and populates the vector that will act as internal buffer.
        /// This doesn't need to be efficient, it's done only once.
        fn create(contents_bytes: &[u8]) -> Vec<u8> {
            let mut buffer = vec![];
            buffer.extend(GitTreeObject::get_object_header_bytes(contents_bytes));
            buffer.extend(contents_bytes);
            buffer
        }

        /// Instantiates the random number generator.
        fn create_rng() -> StdRng {
            match env::var("RNG_TYPE") {
                Ok(value) => match value.as_str() {
                    "PREDICTABLE" => StdRng::seed_from_u64(0),
                    _ => StdRng::from_entropy(),
                },
                Err(_) => StdRng::from_entropy(),
            }
        }

        /// Create a GitCommitObject and returns it.
        pub fn from_tree_bytes(commit_bytes: &[u8], file_sha1: &Vec<u8>) -> Self {
            assert_eq!(file_sha1.len(), 20);
            let internal_buffer = GitTreeObject::create(commit_bytes);
            let sha1_hasher = sha1::Sha1::new();
            let index_sha1 = get_index_sha1(commit_bytes, file_sha1);
            GitTreeObject { buffer: internal_buffer, sha1_hasher, index_file_sha1: index_sha1 }
        }

        /// Calculates sha1 of the tree object
        pub fn sha1(&mut self) -> Vec<u8> {
            self.sha1_hasher.update(&self.buffer);
            self.sha1_hasher.finalize_fixed_reset().to_vec()
        }

        /// Update the sha1 of the file
        pub fn update_file_sha1(&mut self, new_file_sha1: Vec<u8>) {
            assert_eq!(new_file_sha1.len(), 20);
            self.buffer[self.index_file_sha1..self.index_file_sha1 + 20].copy_from_slice(&new_file_sha1);
        }

        /// Returns the commit object CONTENTS (that is, excluding the HEADER)
        pub fn get_tree_object_contents(&self) -> &[u8] {
            let first_null = self.buffer.iter().position(|&x| x == b'\x00').expect("NULL not found in tree object");
            &self.buffer[first_null + 1..]
        }
    }

    #[test]
    fn test_tree_object() -> Result<(), Box<dyn Error>> {
        const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";
        let repository = &mut Repository::open("./test-repo")?;
        reset_repository("./test-repo", COMMIT_HEAD).expect("Reset failed");
        let odb = repository.odb()?;

        const TREE_SHA1: &str = "dc58401aab97aa0d1c279265d535c296d6c4c9f9";
        let tree_object = odb.read(Oid::from_str(TREE_SHA1)?)?;

        let tree_object = repository.find_tree(Oid::from_str(TREE_SHA1)?)?;
        let file2_entry = tree_object.get_name("file2").unwrap();
        assert_eq!(file2_entry.name().unwrap(), "file2");

        const BLOB_SHA1: &str = "7a4596f15adadab08ef8b71594d3692f177fce9e";

        let odb_object = odb.read(tree_object.id())?;
        let tree_object_contents_bytes = odb_object.data();
        let looking_for = hex::decode(BLOB_SHA1)?;

        let mut git_tree_ob = GitTreeObject::from_tree_bytes(tree_object_contents_bytes, &looking_for);
        assert_eq!(hex::encode(git_tree_ob.sha1()), TREE_SHA1);

        git_tree_ob.update_file_sha1(hex::decode("0011001100110011001100110011001100110011")?);

        assert_eq!(hex::encode(git_tree_ob.sha1()), "5556559755027d61ef8a50935d293f0f4218b7d8");

        let new_tree_sha1 = odb.write(ObjectType::Tree, git_tree_ob.get_tree_object_contents())?;
        assert_eq!(hex::encode(new_tree_sha1.as_bytes()), "5556559755027d61ef8a50935d293f0f4218b7d8");

        let tree_object = odb.read(Oid::from_str("5556559755027d61ef8a50935d293f0f4218b7d8")?)?;

        Ok(())
    }

    #[test]
    fn discover_blob() -> Result<(), Box<dyn Error>> {
        let repository = &mut Repository::open("./test-repo")?;
        reset_repository("./test-repo", COMMIT_HEAD).expect("Reset failed");
        let odb = repository.odb()?;
        let blob_object = odb.read(Oid::from_str(BLOB_SHA1)?)?;

        pub struct GitBlobObject {
            pub buffer: Vec<u8>,
            sha1_hasher: sha1::Sha1,
            index_contents: usize,
            rng: StdRng,
        }

        impl GitBlobObject {
            /// Create a GitCommitObject and returns it.
            pub fn from_size(size: usize) -> Self {
                assert!(size >= 10);
                let header = format!("blob {}\x00", size);
                let header = header.as_bytes();

                let mut buffer = Vec::with_capacity(header.len() + size);
                buffer.extend(header);
                buffer.extend(vec![0u8; size]);

                let sha1_hasher = sha1::Sha1::new();
                let index_contents = header.len();
                GitBlobObject { buffer, sha1_hasher, index_contents, rng: GitBlobObject::create_rng() }
            }

            pub fn from_size_predictable(size: usize) -> Self {
                let mut instance = GitBlobObject::from_size(size);
                instance.rng = StdRng::seed_from_u64(0);
                instance
            }

            /// Instantiates the random number generator.
            fn create_rng() -> StdRng {
                match env::var("RNG_TYPE") {
                    Ok(value) => match value.as_str() {
                        "PREDICTABLE" => StdRng::seed_from_u64(0),
                        _ => StdRng::from_entropy(),
                    },
                    Err(_) => StdRng::from_entropy(),
                }
            }

            pub fn generate_random_contents(&mut self) {
                self.rng.fill_bytes(&mut self.buffer[self.index_contents..]);
            }

            /// Calculates sha1 of the blob object
            pub fn sha1(&mut self) -> Vec<u8> {
                self.sha1_hasher.update(&self.buffer);
                self.sha1_hasher.finalize_fixed_reset().to_vec()
            }

            /// Returns the commit object CONTENTS (that is, excluding the HEADER)
            pub fn get_blob_object_contents(&self) -> &[u8] {
                &self.buffer[self.index_contents..]
            }

            pub fn make_predictable(&mut self) {
                self.rng = StdRng::seed_from_u64(0);
            }
        }

        let odb_object = odb.read(blob_object.id())?;
        let blob_object_contents = Vec::from(odb_object.data());

        let mut dyn_blob_object = GitBlobObject::from_size_predictable(10);
        assert_eq!(
            hex::encode(dyn_blob_object.sha1()),
            hex::encode(calculate_sha1(&vec![
                b'b', b'l', b'o', b'b', b' ', b'1', b'0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            ]))
        );
        assert_eq!(dyn_blob_object.get_blob_object_contents(), &vec![0u8; 10]);

        dyn_blob_object.generate_random_contents();
        assert_eq!(hex::encode(dyn_blob_object.sha1()), "99893b920710bb20c0512762ea9fbf35d6424cf8");

        dyn_blob_object.generate_random_contents();
        assert_eq!(hex::encode(dyn_blob_object.sha1()), "8fd54796c0ec88013d13398aa4036374d1285bf1");

        let new_blob_sha1 = odb.write(ObjectType::Blob, dyn_blob_object.get_blob_object_contents())?;
        assert_eq!(hex::encode(new_blob_sha1.as_bytes()), "8fd54796c0ec88013d13398aa4036374d1285bf1");

        let _blob_object = odb.read(Oid::from_str("8fd54796c0ec88013d13398aa4036374d1285bf1")?)?;

        Ok(())
    }
}
