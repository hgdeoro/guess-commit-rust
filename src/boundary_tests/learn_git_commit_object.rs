#[cfg(test)]
mod tests {
    use crate::utils::{reset_repository, MUT_BEGIN, MUT_END_INCLUSIVE};
    use sha1::{Digest, Sha1};

    pub fn calculate_sha1(buffer: &Vec<u8>) -> Vec<u8> {
        let mut sha1_hasher = Sha1::new();
        sha1_hasher.update(buffer);
        let sha1_result: Vec<u8> = sha1_hasher.finalize().to_vec();
        sha1_result
    }

    #[test]
    fn test_check_test_repo() {
        use git2::{ObjectType, Oid, Repository, ResetType};

        const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";
        const COMMIT_EXPECTED: &str = "caddaae7961dd28310ff46d74bbe3d0578475362";

        reset_repository("./test-repo", COMMIT_HEAD).expect("Reset failed");

        let repository = &mut Repository::open("./test-repo").unwrap();
        let commit_oid = Oid::from_str(COMMIT_HEAD).unwrap();

        let repository_head = repository.head().unwrap();
        let commit = repository_head.peel_to_commit().unwrap();

        assert_eq!(format!("{}", commit.id()), COMMIT_HEAD);
        assert_eq!(
            commit.message().unwrap(),
            "Update file2\n\nSecond line\n************************************************************\n"
        );
        assert_eq!(
            String::from_utf8(commit.message_raw_bytes().to_vec()).unwrap(),
            "Update file2\n\nSecond line\n************************************************************\n"
        );

        let odb = repository.odb().unwrap();
        let commit_odb_object = odb.read(commit_oid).unwrap();
        assert_eq!(commit_odb_object.data().len(), 309);

        let commit_bytes = commit_odb_object.data();

        // println!(">>>\n{}\n<<<", String::from_utf8_lossy(commit_bytes));

        // now get commit ID
        let commit_size = commit_bytes.len();
        let commit_size_string = format!("{}", commit_size);
        assert_eq!(commit_size_string, "309");

        let header_pre_null_string = format!("commit {}", commit_size);
        let buffer_len = header_pre_null_string.len() + 1 + commit_size;
        let mut buffer = vec![0_u8; buffer_len];

        // add 'commit ' + $len to buffer
        for (pos, value) in header_pre_null_string.as_bytes().iter().enumerate() {
            buffer[pos] = *value;
        }

        // add NULL separator to buffer
        buffer[7 + commit_size_string.len()] = 0;

        // copy commit bytes
        let start_pos = 7 + commit_size_string.len() + 1;
        assert_eq!(7 + commit_size_string.len() + 1 + commit_size, buffer.len());

        for (pos, value) in commit_bytes.iter().enumerate() {
            buffer[start_pos + pos] = *value;
        }

        // calculate sha1 to make sure we did everything well
        let sha1_result = calculate_sha1(&buffer);
        assert_eq!(sha1_result[..], hex::decode("a2d4044ee8bdeb0d6620134d94b13584ed80bd2b").unwrap());

        // let's see where mutable zone starts...
        let mutable_start_pos = 7 + commit_size_string.len() + 1 + commit_size - 60 - 1;
        for (pos, value) in "************************************************************".as_bytes().iter().enumerate()
        {
            assert_eq!(buffer[mutable_start_pos + pos], *value);
        }

        // reset mutable zone with values: 33-126
        for pos in 0..60 {
            buffer[mutable_start_pos + pos] = MUT_BEGIN;
        }
        for (pos, value) in "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!".as_bytes().iter().enumerate()
        {
            assert_eq!(buffer[mutable_start_pos + pos], *value);
        }

        // We ned to transform wanted prefix into bytes
        let wanted_prefix_str = String::from("ca");
        let wanted_prefix_bytes = hex::decode(wanted_prefix_str).unwrap();
        let mut found = false;

        for _ in 1..1000 {
            // update mutable zone
            buffer[mutable_start_pos] += 1;
            for i in 0..60 {
                if buffer[mutable_start_pos + i] > MUT_END_INCLUSIVE {
                    buffer[mutable_start_pos + i] = MUT_BEGIN;
                    buffer[mutable_start_pos + i + 1] += 1;
                } else {
                    break;
                }
            }

            // calculate sha1
            let sha1_result = calculate_sha1(&buffer);

            // check if we found a match
            found = true;
            for (i, value) in wanted_prefix_bytes.iter().enumerate() {
                if sha1_result[i] != *value {
                    found = false;
                    break;
                }
            }

            if found {
                break;
            }
        }

        assert!(found);

        // Create commit object from buffer
        let new_oid = odb.write(ObjectType::Commit, &buffer[7 + commit_size_string.len() + 1..]).unwrap();
        println!("GUESSED OID: {}", new_oid);
        assert_eq!(format!("{}", new_oid), COMMIT_EXPECTED);

        // Update branch
        repository
            .reset(repository.find_commit(new_oid).unwrap().as_object(), ResetType::Soft, Option::None)
            .expect("amend failed");

        // Check if head is pointing to the new commit
        assert_eq!(format!("{}", repository.head().unwrap().peel_to_commit().unwrap().id()), COMMIT_EXPECTED);
    }
}
