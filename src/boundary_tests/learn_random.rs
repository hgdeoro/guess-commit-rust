#[cfg(test)]
mod tests {
    use std::time::Instant;

    use rand::rngs::StdRng;
    use rand_core::{RngCore, SeedableRng};

    #[test]
    fn test_predictable_random_generation() {
        let mut rng = StdRng::seed_from_u64(0);
        assert_eq!(rng.next_u32(), 3442241407);
        assert_eq!(rng.next_u32(), 3140108210);

        let mut rng = StdRng::seed_from_u64(0);
        assert_eq!(rng.next_u32(), 3442241407);
        assert_eq!(rng.next_u32(), 3140108210);
    }

    #[test]
    fn test_unpredictable_random_generation() {
        let mut rng = StdRng::from_entropy();
        let value_1 = rng.next_u32();

        let mut rng = StdRng::from_entropy();
        let value_2 = rng.next_u32();

        assert_ne!(value_1, value_2)
    }

    #[test]
    #[ignore]
    fn test_perf_rng() {
        // Seems like fill_bytes() is faster
        // next_u64(): ----->               259857/sec
        // fill_bytes(): --->               377086/sec
        for _ in 0..5 {
            // --------------------
            // next_u64()
            // --------------------

            let mut rng = StdRng::from_entropy();
            let start_time = Instant::now();
            let iters = 100000;
            let mut buffer = [0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8];

            for _ in 0..iters {
                let value = rng.next_u64();
                for i in 0..8 {
                    buffer[i] = ((value >> (8 * i)) & 0xff) as u8;
                }
            }

            let elapsed = start_time.elapsed().as_secs_f32();
            println!("next_u64(): -----> {:>20.0}/sec", (iters as f32 / elapsed));

            // --------------------
            // fill_bytes()
            // --------------------

            let mut rng = StdRng::from_entropy();
            let mut random_bytes = [0u8; 8];
            let start_time = Instant::now();
            let mut buffer = [0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8];

            for _ in 0..iters {
                rng.fill_bytes(&mut random_bytes);
                buffer[..8].copy_from_slice(&random_bytes[..8]);
            }
            let elapsed = start_time.elapsed().as_secs_f32();
            println!("fill_bytes(): ---> {:>20.0}/sec", (iters as f32 / elapsed));

            // --------------------
            // fill_bytes2()
            // --------------------

            let mut rng = StdRng::from_entropy();
            let start_time = Instant::now();
            let mut buffer = [0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8];

            for _ in 0..iters {
                rng.fill_bytes(&mut buffer[0..8]);
            }
            let elapsed = start_time.elapsed().as_secs_f32();
            println!("fill_bytes2(): --> {:>20.0}/sec", (iters as f32 / elapsed));
        }
    }
}
