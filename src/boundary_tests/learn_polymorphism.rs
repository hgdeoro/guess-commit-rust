use git2::{self};
use rand::rngs::StdRng;
use rand_core::SeedableRng;
use sha1::{self, Digest};
use std::env;

#[allow(dead_code, unused_variables)]
pub trait DynaGitObject {
    fn get_object_header_bytes(object: &git2::OdbObject) -> Vec<u8> {
        match object.kind() {
            git2::ObjectType::Tree => format!("commit {}\0", object.len()).as_bytes().to_vec(),
            git2::ObjectType::Blob => format!("blob {}\0", object.len()).as_bytes().to_vec(),
            git2::ObjectType::Commit => format!("commit {}\0", object.len()).as_bytes().to_vec(),
            other => panic!("Unexpected type of OdbObject. type={:?}", other),
        }
    }

    fn create_buffer(repository: &git2::Repository, oid: &git2::Oid) -> Vec<u8> {
        let odb = repository.odb().unwrap();

        let commit_odb_object = odb.read(*oid).expect("Commit not found in odb");
        let commit_bytes = commit_odb_object.data();
        assert_eq!(commit_odb_object.len(), commit_bytes.len()); // we assume this on get_object_header_bytes()

        let mut internal_buffer = vec![];
        internal_buffer.extend(Self::get_object_header_bytes(&commit_odb_object));
        internal_buffer.extend(commit_bytes);
        internal_buffer
    }

    fn find_string_in_buffer(internal_buffer: &[u8], to_find: String) -> usize {
        0
    }

    fn create_rng() -> StdRng {
        match env::var("RNG_TYPE") {
            Ok(value) => match value.as_str() {
                "PREDICTABLE" => StdRng::seed_from_u64(0),
                _ => StdRng::from_entropy(),
            },
            Err(_) => StdRng::from_entropy(),
        }
    }
}

#[allow(dead_code)]
pub struct GitCommitObject {
    pub buffer: Vec<u8>,
    pub mutable_zone_start: usize, // FIXME: Option<usize> would be better. Check performance.
    pub mutable_zone_size: usize,  // FIXME: Option<usize> would be better. Check performance.
    sha1_hasher: sha1::Sha1,
    rng: StdRng,
    index_tree_sha1: usize,
}

impl DynaGitObject for GitCommitObject {}

#[allow(dead_code)]
impl GitCommitObject {
    pub fn from_repository(repository: &git2::Repository, oid: &git2::Oid) -> GitCommitObject {
        let internal_buffer = GitCommitObject::create_buffer(repository, oid);
        let (mutable_zone_start, mutable_zone_size) =
            GitCommitObject::get_mutable_zone_start_and_len(&internal_buffer).unwrap_or((0, 0));
        let index_tree_sha1 = GitCommitObject::find_string_in_buffer(&internal_buffer, String::from("ok"));

        let sha1_hasher = sha1::Sha1::new();
        let rng = GitCommitObject::create_rng();

        GitCommitObject {
            buffer: internal_buffer,
            mutable_zone_start,
            mutable_zone_size,
            sha1_hasher,
            rng,
            index_tree_sha1,
        }
    }

    /// Returns information to locate mutable zone inside the buffer.
    /// The mutable zone is the zone that initially contains consecutive '*'.
    /// That zone will be replaced with random ASCII characters.
    pub fn get_mutable_zone_start_and_len(buffer: &Vec<u8>) -> Result<(usize, usize), ()> {
        let mut start_pos = 0;
        let mut count = 0;
        for (pos, value) in buffer.iter().enumerate() {
            if *value == b'*' {
                if count == 0 {
                    // we found first one
                    count = 1;
                    start_pos = pos;
                } else {
                    // we found a serie :)
                    count += 1;
                }
            } else {
                // not a '*', maybe we found the end of it
                if pos - start_pos >= 10 {
                    // yeap, we found the end of it :D
                    return Ok((start_pos, pos - start_pos));
                } else {
                    count = 0;
                    start_pos = pos;
                }
            }
        }
        if buffer.len() - start_pos >= 10 {
            Ok((start_pos, buffer.len() - start_pos))
        } else {
            Err(())
        }
    }
}
