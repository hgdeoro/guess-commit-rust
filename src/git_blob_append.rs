use crate::git_base::DynaGitObject;
use anyhow::{anyhow, Result};
use rand::rngs::StdRng;
use rand_core::RngCore;
use sha1::digest::FixedOutputReset;
use sha1::{self, Digest};

pub struct GitBlobAppendObject {
    pub buffer: Vec<u8>,
    sha1_hasher: sha1::Sha1,
    rng: StdRng,
    append_size: usize,
}

impl DynaGitObject for GitBlobAppendObject {}

impl GitBlobAppendObject {
    /// Create a GitCommitObject and returns it.
    pub fn from_existing_file(
        repository: &git2::Repository,
        oid: &git2::Oid,
        filename: String,
        append_size: usize,
    ) -> Result<Self> {
        assert!(append_size >= 10);

        let tree_id = repository.find_commit(*oid)?.tree_id();
        let tree = repository.find_tree(tree_id)?;
        let entry = match tree.get_name(&filename) {
            Some(entry) => entry,
            _ => return Err(anyhow!("File {} not found in tree {}", filename, tree_id)),
        };
        let blob = repository.find_blob(entry.id())?;
        let file_contents = blob.content();

        // contents_size: original contents + appended random values
        let contents_size = file_contents.len() + append_size;
        let header = format!("blob {}\x00", contents_size);
        let header = header.as_bytes();
        let buffer_size = header.len() + contents_size;

        let mut buffer = Vec::with_capacity(buffer_size);
        buffer.extend(header);
        buffer.extend(file_contents);
        buffer.extend(vec![0u8; append_size]);

        assert_eq!(buffer.len(), buffer_size);

        let mut sha1_hasher = sha1::Sha1::new();
        sha1_hasher.update(&buffer[0..buffer_size - append_size]);

        let rng = Self::create_rng();

        Ok(GitBlobAppendObject { buffer, sha1_hasher, rng, append_size })
    }

    pub fn generate_random_contents(&mut self) {
        let start_pos = self.buffer.len() - self.append_size;
        self.rng.fill_bytes(&mut self.buffer[start_pos..]);
    }

    /// Calculates sha1 of the blob object
    pub fn sha1(&self) -> Vec<u8> {
        let start_pos = self.buffer.len() - self.append_size;
        let mut incremental_sha1 = self.sha1_hasher.clone();
        incremental_sha1.update(&self.buffer[start_pos..]);
        incremental_sha1.finalize_fixed_reset().to_vec()
    }

    /// Returns the commit object CONTENTS (that is, excluding the HEADER)
    pub fn get_blob_object_contents(&self) -> Result<&[u8]> {
        Self::contents_from_buffer(&self.buffer)
    }
}

#[cfg(test)]
mod tests {
    use crate::utils::reset_repository;
    use git2::{ObjectType, Oid};
    use std::env;

    use super::*;

    const BIGFILE_COMMIT: &str = "c7a2ca90d11144ecfcafc77e0eed03fcd7ae244b";
    const BIGFILE_FILENAME: &str = "installer.tar.gz";
    const EXPECTED_SHA1: &str = "6da2fb3e3bb0094bc13585e5e1599c21ea5ae4ee";

    #[test]
    fn test_from_existing_file() -> Result<()> {
        env::set_var("RNG_TYPE", "PREDICTABLE");

        let repository = reset_repository("./test-repo", BIGFILE_COMMIT)?;
        let oid = git2::Oid::from_str(BIGFILE_COMMIT)?;
        let filename = String::from(BIGFILE_FILENAME);

        let mut dyn_blob_object = GitBlobAppendObject::from_existing_file(&repository, &oid, filename, 64)?;
        let start_pos_zeros = dyn_blob_object.buffer.len() - 64;
        assert_eq!(&dyn_blob_object.buffer[start_pos_zeros..], vec![0u8; 64]);
        assert_eq!(hex::encode(dyn_blob_object.sha1()), "a2e577edbc8fdbff5c4e55bdf60b6618643fe331");

        dyn_blob_object.generate_random_contents();
        assert_eq!(hex::encode(dyn_blob_object.sha1()), EXPECTED_SHA1);

        // let's write it
        let odb = repository.odb()?;
        let new_blob_sha1 = odb.write(ObjectType::Blob, dyn_blob_object.get_blob_object_contents()?)?;
        assert_eq!(hex::encode(new_blob_sha1.as_bytes()), EXPECTED_SHA1);

        let _blob_object = odb.read(Oid::from_str(EXPECTED_SHA1)?)?;

        Ok(())
    }
}
