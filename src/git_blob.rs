use crate::git_base::DynaGitObject;
use anyhow::Result;
use rand::rngs::StdRng;
use rand_core::RngCore;
use sha1::digest::FixedOutputReset;
use sha1::{self, Digest};

pub struct GitBlobObject {
    pub buffer: Vec<u8>,
    sha1_hasher: sha1::Sha1,
    index_contents: usize,
    rng: StdRng,
}

impl DynaGitObject for GitBlobObject {}

impl GitBlobObject {
    /// Create a GitCommitObject and returns it.
    pub fn from_size(size: usize) -> Self {
        assert!(size >= 10);
        let header = format!("blob {}\x00", size);
        let header = header.as_bytes();

        let mut buffer = Vec::with_capacity(header.len() + size);
        buffer.extend(header);
        buffer.extend(vec![0u8; size]);

        let sha1_hasher = sha1::Sha1::new();
        let index_contents = header.len();
        let rng = Self::create_rng();
        GitBlobObject { buffer, sha1_hasher, index_contents, rng }
    }

    pub fn generate_random_contents(&mut self) {
        self.rng.fill_bytes(&mut self.buffer[self.index_contents..]);
    }

    /// Calculates sha1 of the blob object
    pub fn sha1(&mut self) -> Vec<u8> {
        self.sha1_hasher.update(&self.buffer);
        self.sha1_hasher.finalize_fixed_reset().to_vec()
    }

    /// Returns the commit object CONTENTS (that is, excluding the HEADER)
    pub fn get_blob_object_contents(&self) -> Result<&[u8]> {
        Self::contents_from_buffer(&self.buffer)
    }
}

#[cfg(test)]
mod tests {
    use crate::utils::{calculate_sha1, reset_repository};
    use git2::{ObjectType, Oid};
    use std::env;

    use super::*;

    const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";

    #[test]
    fn test() -> Result<()> {
        env::set_var("RNG_TYPE", "PREDICTABLE");

        let repository = reset_repository("./test-repo", COMMIT_HEAD)?;
        let odb = repository.odb()?;
        let mut dyn_blob_object = GitBlobObject::from_size(10);

        assert_eq!(
            hex::encode(dyn_blob_object.sha1()),
            hex::encode(calculate_sha1(&b"blob 10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00".to_vec()))
        );
        assert_eq!(dyn_blob_object.get_blob_object_contents()?, &vec![0u8; 10]);

        dyn_blob_object.generate_random_contents();
        assert_eq!(hex::encode(dyn_blob_object.sha1()), "99893b920710bb20c0512762ea9fbf35d6424cf8");

        dyn_blob_object.generate_random_contents();
        assert_eq!(hex::encode(dyn_blob_object.sha1()), "8fd54796c0ec88013d13398aa4036374d1285bf1");

        let new_blob_sha1 = odb.write(ObjectType::Blob, dyn_blob_object.get_blob_object_contents()?)?;
        assert_eq!(hex::encode(new_blob_sha1.as_bytes()), "8fd54796c0ec88013d13398aa4036374d1285bf1");

        let _blob_object = odb.read(Oid::from_str("8fd54796c0ec88013d13398aa4036374d1285bf1")?)?;

        Ok(())
    }
}
