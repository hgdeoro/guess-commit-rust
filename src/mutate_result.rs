extern crate core;

use thiserror::{self};

#[derive(Debug)]
pub struct MutateResult {
    pub commit_sha1: Vec<u8>,
    pub iterations: u64,
    pub duration_sec: f64,
}

#[derive(thiserror::Error, Debug, PartialEq, Eq)]
pub enum GuessCommitError {
    #[error("directory doesn't contains a valid git repository")]
    InvalidRepository,

    #[error("couldn't find the wanted prefix")]
    MaxIterationsError,

    #[error("couldn't reset HEAD to specified commit")]
    InvalidCurrentCommit,

    #[error("an internal error occurred")]
    InternalError,

    #[error("the commit message doesn't contains a mutable zone")]
    MutableZoneNotFound,
}
