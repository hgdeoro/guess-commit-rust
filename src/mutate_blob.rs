extern crate core;

use std::time::{self};

use git2::{self};

use crate::git_blob;
use crate::git_commit;
use crate::git_tree;

use crate::mutate_result::{GuessCommitError, MutateResult};
use anyhow::Result;

pub fn guess_by_mutating_blob(
    repository: git2::Repository,
    commit_oid: git2::Oid,
    max_iterations: u64,
    wanted_prefix: Vec<u8>,
    dry_run: bool,
    filename: &str,
    blob_size: usize,
) -> Result<MutateResult> {
    let mut dyn_tree = git_tree::GitTreeObject::from_repository(&repository, &commit_oid, String::from(filename))?;
    let mut dyn_commit = git_commit::GitCommitObject::from_repository(&repository, &commit_oid)?;
    let mut dyn_blob = git_blob::GitBlobObject::from_size(blob_size);

    let prog_start_time = time::Instant::now();
    let mut prog_last_print: u64 = 0;
    let mut iterations = 0;
    loop {
        iterations += 1;

        dyn_blob.generate_random_contents();

        // We need to update tree object with new sha1 of blob
        dyn_tree.update_file_sha1(dyn_blob.sha1());

        dyn_commit.update_tree_sha1_bytes(dyn_tree.sha1());

        // calculate sha1 and check if we found a match
        if dyn_commit.calculate_sha1_and_compare(&wanted_prefix) {
            break;
        }

        if iterations >= max_iterations {
            return Err(anyhow::anyhow!(GuessCommitError::MaxIterationsError));
        }
        if iterations % 10000 == 0 {
            let elapsed = prog_start_time.elapsed().as_secs_f64();
            let elapsed_int = elapsed as u64;
            if elapsed_int > prog_last_print {
                prog_last_print = elapsed_int;
                println!(" + so far: {:.2?} iterations/sec", iterations as f64 / elapsed);
            }
        }
    }

    let new_commit_oid;
    if dry_run {
        println!("Won't write commit (--dry-run)");
        new_commit_oid = git2::Oid::from_bytes(&dyn_commit.sha1())?;
    } else {
        // Insert commit object with contents from buffer
        let odb = repository.odb()?;
        let new_blob_oid = odb.write(git2::ObjectType::Blob, dyn_blob.get_blob_object_contents()?)?;
        let new_tree_oid = odb.write(git2::ObjectType::Tree, dyn_tree.get_tree_object_contents()?)?;
        new_commit_oid = odb.write(git2::ObjectType::Commit, dyn_commit.get_commit_object_contents()?)?;

        assert!(odb.exists(new_blob_oid));
        assert!(odb.exists(new_tree_oid));
        assert!(odb.exists(new_commit_oid));

        repository.find_blob(new_blob_oid).expect("Attempt to read created BLOB failed :(");
        repository.find_tree(new_tree_oid).expect("Attempt to read created TREE failed :(");
        repository.find_commit(new_commit_oid).expect("Attempt to read created COMMIT failed :(");

        // Update HEAD to point to the commit we just created
        repository
            .reset(repository.find_commit(new_commit_oid).unwrap().as_object(), git2::ResetType::Soft, Option::None)
            .expect("amend failed");
    }
    return Ok(MutateResult {
        commit_sha1: new_commit_oid.as_bytes().to_vec(),
        iterations,
        duration_sec: prog_start_time.elapsed().as_secs_f64(),
    });
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils;

    const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";
    const COMMIT_EXPECTED: &str = "cae86a52a170344cf8fa7664329cb3a11005fa48";

    #[test]
    fn test_guess_commit() -> Result<()> {
        std::env::set_var("RNG_TYPE", "PREDICTABLE");

        let repository = utils::reset_repository("./test-repo", COMMIT_HEAD)?;
        let commit_oid = git2::Oid::from_str(COMMIT_HEAD)?;
        let max_iterations = 10_000;
        let wanted_prefix = hex::decode("ca")?;
        let filename = "file2";
        let blob_size = 64;

        let result =
            guess_by_mutating_blob(repository, commit_oid, max_iterations, wanted_prefix, false, filename, blob_size)?;
        assert_eq!(hex::encode(result.commit_sha1), COMMIT_EXPECTED);

        Ok(())
    }
}
