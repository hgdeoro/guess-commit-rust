use std::ascii;

use anyhow::Result;
use git2::{self};
use sha1::digest::FixedOutputReset;
use sha1::{self, Digest};

pub const MUT_BEGIN: u8 = 33;
pub const MUT_END_INCLUSIVE: u8 = 126;
pub const MUT_WIDTH: u8 = MUT_END_INCLUSIVE - MUT_BEGIN + 1;

/// Returns String representation of `git2::Commit`
// FIXME: is there any way to provide this functionality by implementing a trait, so this method doesn't need to even exists?
pub fn hex_commit(commit: &git2::Commit) -> String {
    hex::encode(commit.as_object().id().as_bytes())
}

/// Resets repository to make HEAD point to specified commit
pub fn reset_repository(repository_path: &str, commit_id: &str) -> Result<git2::Repository> {
    let repository = git2::Repository::open(repository_path)?;
    let commit_oid = git2::Oid::from_str(commit_id)?;
    let commit = repository.find_commit(commit_oid)?;
    repository.reset(commit.as_object(), git2::ResetType::Soft, Option::None).expect("reset failed");

    let repository = git2::Repository::open(repository_path)?;
    let oid = repository.resolve_reference_from_short_name("HEAD")?.target().unwrap();
    assert_eq!(hex::encode(oid.as_bytes()), commit_id);

    Ok(repository)
}

pub fn calculate_sha1(buffer: &Vec<u8>) -> Vec<u8> {
    let mut sha1_hasher = sha1::Sha1::new();
    sha1_hasher.update(buffer);
    sha1_hasher.finalize_fixed_reset().to_vec()
}

pub fn prettify(binary_data: &[u8]) -> String {
    let mut result = String::from("");
    for a_byte in binary_data {
        if *a_byte == b'\n' {
            result.push('\n');
        } else {
            for item in ascii::escape_default(*a_byte) {
                result.push(item as char);
            }
        }
    }
    result
}

const HEX_CHARS: &[u8; 16] = b"0123456789abcdef";

pub fn write_hex_string(output_buffer: &mut [u8], input_buffer: &[u8]) {
    debug_assert_eq!(output_buffer.len(), input_buffer.len() * 2);
    for i in 0..input_buffer.len() {
        let low = (input_buffer[i] / 16) as usize;
        let high = (input_buffer[i] % 16) as usize;
        output_buffer[i * 2] = HEX_CHARS[low];
        output_buffer[(i * 2) + 1] = HEX_CHARS[high];
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const COMMIT_HEAD: &str = "a2d4044ee8bdeb0d6620134d94b13584ed80bd2b";
    const ANOTHER_COMMIT: &str = "3abdd98c08a881ae47743b01c9afdc92a1702f09";

    #[test]
    fn test_reset() -> Result<()> {
        reset_repository("./test-repo", COMMIT_HEAD).expect("Reset failed");
        let repository = &mut git2::Repository::open("./test-repo")?;
        assert_eq!(format!("{}", repository.head()?.peel_to_commit()?.id()), COMMIT_HEAD);

        reset_repository("./test-repo", ANOTHER_COMMIT).expect("Reset failed");
        let repository = &mut git2::Repository::open("./test-repo")?;
        assert_eq!(format!("{}", repository.head()?.peel_to_commit()?.id()), ANOTHER_COMMIT);

        reset_repository("./test-repo", COMMIT_HEAD).expect("Reset failed");
        let repository = &mut git2::Repository::open("./test-repo")?;
        assert_eq!(format!("{}", repository.head()?.peel_to_commit()?.id()), COMMIT_HEAD);

        Ok(())
    }

    #[test]
    fn test_prettify() {
        let result = prettify(b"Some\x00\nitem");
        println!("{}", result);
        assert_eq!(result, String::from("Some\\x00\nitem"));
    }

    #[test]
    fn test_hex_push() {
        let input_bytes = b"\xff\x00\x55\x22";
        let output_string = "ff005522";

        assert_eq!(hex::encode(input_bytes), output_string);
        assert_eq!(input_bytes.len(), 4);
        assert_eq!(output_string.len(), 8);

        let mut output_buffer = vec![b'*'; 12];
        assert_eq!(output_buffer, b"************");

        write_hex_string(&mut output_buffer[2..10], input_bytes);
        assert_eq!(output_buffer, b"**ff005522**");
    }
}
